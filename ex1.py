from skimage import io
import numpy as np

def negative(img):
    '''
        Returns the negative of a binary uint8 image
        using the not binary operator
    '''
    return ~img

def transform(img, min, max):
    '''
        Transform the image to the new range of values (min, max)
    '''
    img = img * ((max-min)/255)
    return img

def invert_pair_rows(img):
    '''
        Returns the image with the pair rows inverted from right to left
    '''
    img = [img[i][::-1] if i%2==0 else img[i] for i in range(len(img))]
    return np.asarray(img)

def middle_image_mirror(img):
    '''
        Apply a mirroring effect in the middle of the image
    '''
    middle = img.shape[0]//2
    img = [img[middle-(i-middle)] if i >= middle else img[i] for i in range(len(img))]
    return np.asarray(img)


if __name__ == '__main__':
    img = io.imread('../images/city.png')
    print("Shape: ", img.shape, " Type: ", img.dtype)

    neg = negative(img)
    io.imsave('negative.png',neg)

    tranformed = transform(img, 100, 200)
    io.imsave('transform_100_200.png',tranformed)

    inverted = invert_pair_rows(img)
    io.imsave('pair_rows_inverted.png',inverted)

    mirrored = middle_image_mirror(img)
    io.imsave('mirrored.png',mirrored)

    #io.imshow(img)
    #io.show()
