from skimage import io
import numpy as np

def extract_bit_plane(img, bit_pos):
    byte = np.asarray([0 if i != bit_pos else 1*2**i for i in range(8)]).sum() #get int value
    byte = byte.astype(np.uint8) #converting to byte
    return img & byte

if __name__ == '__main__':
    img = io.imread('../images/baboon.png')
    print("Shape: ", img.shape, " Type: ", img.dtype)
    bit_pos = 4

    img = extract_bit_plane(img, bit_pos)
    io.imsave("bit_"+str(bit_pos)+".png",img)
    io.imshow(img)
    io.show()
