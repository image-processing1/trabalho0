from skimage import io
import numpy as np

def gamma_correction(img, gamma):
    img = img * (1/255) #convert to the interval (0,1)
    img = img**(1/gamma) #apply gamma
    img = img * 255 #convert to the interval (0,255)

    return img

if __name__ == '__main__':
    img = io.imread('../images/baboon.png')
    print("Shape: ", img.shape, " Type: ", img.dtype)
    gamma = 0.5
    img = gamma_correction(img, gamma)
    io.imsave("gamma_"+str(gamma)+".png",img)
    #io.imshow(img)
    #io.show()
